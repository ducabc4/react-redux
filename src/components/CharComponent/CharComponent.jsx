import React from 'react';

import './style.scss';

const CharComponent = props => {
	return (
		<button className="char-component" onClick={props.onClickChar}>
			{props.letter}
		</button>
	);
};

export default CharComponent;
