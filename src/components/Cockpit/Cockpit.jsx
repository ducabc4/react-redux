import React, { useEffect, useContext, useRef } from 'react';
import styled from 'styled-components';

import AuthContext from '../../context/auth-context';

const StyledButton = styled.button`
	color: white;
	font: inherit;
	border: 1px solid blue;
	padding: 8px;
	cursor: pointer;
	margin-right: 10px;

	background-color: ${props => (props[props.type] ? 'green' : 'red')};
	&:hover {
		background-color: ${props => (props[props.type] ? 'lightgreen' : 'salmon')};
	}
`;

const Cockpit = props => {
	const toggleButtonRef = useRef(null);

	const authContext = useContext(AuthContext);

	useEffect(() => {
		const timer = setTimeout(
			() => console.log('[Cockpit.jsx] ComponentDidMount'),
			1000
		);

		toggleButtonRef.current.click();

		return () => {
			clearTimeout(timer);
		};
	}, []);

	useEffect(() => {
		console.log('[Cockpit.jsx] render' + ' ' + props.showPerson);
	}, [props.showPerson, props.onToggleShowPerson]);

	return (
		<React.Fragment>
			<h1>Hi, i am react app</h1>
			<StyledButton
				ref={toggleButtonRef}
				show={props.showPerson}
				onClick={props.onToggleShowPerson}
				type="show"
			>
				Toggle
			</StyledButton>
			<StyledButton
				onClick={authContext.onClickLogin}
				isAuth={authContext.isAuth}
				type="isAuth"
			>
				{authContext.isAuth ? 'Logout' : 'Login'}
			</StyledButton>
		</React.Fragment>
	);
};

export default React.memo(Cockpit);
