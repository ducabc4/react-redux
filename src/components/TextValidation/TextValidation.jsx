import React from 'react';

const TextValidation = props => {
	let textMessage = null;

	if (props.inputText.length < 5) {
		textMessage = 'Text is too short';
	} else if (props.inputText.length > 20) {
		textMessage = 'Text is too long';
	} else {
		textMessage = 'Text is long enough';
	}

	return <p>{textMessage}</p>;
};

export default TextValidation;
