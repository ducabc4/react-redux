import React, { PureComponent } from 'react';
import Person from './Person/Person';
import PropTypes from 'prop-types';

class Persons extends PureComponent {
	render() {
		console.log('[Persons.jsx] rendering...');
		return this.props.persons.map(person => (
			<Person
				key={person.id}
				name={person.name}
				age={person.age}
				onClickDelete={this.props.handleDeletePerson(person.id)}
				onChangeText={this.props.handleChangeName(person.id)}
			/>
		));
	}
}

Persons.propTypes = {
	persons: {
		id: PropTypes.string,
		name: PropTypes.string,
		age: PropTypes.number
	}
};
export default Persons;
