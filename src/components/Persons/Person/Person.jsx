import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import withClass from '../../../hoc/withClass';
import AuthContext from '../../../context/auth-context';

import './person.css';

import classes from './person.css';

const StyledDiv = styled.div`
	width: 60%;
	margin: 16px auto;
	border: 1px solid #eee;
	box-shadow: 0 2px 3px #cccccc;
	padding: 16px;
	text-align: center;

	@media (min-width: 500px) : {
		width: 450px;
	}
`;

class Person extends Component {
	constructor() {
		super();

		this.inputElement = React.createRef();
	}

	static contextType = AuthContext;

	componentDidMount() {
		this.inputElement.current.focus();
	}

	render() {
		return (
			<React.Fragment>
				<p>{this.context.isAuth ? 'Login success' : 'Please login'}</p>
				<p>
					I'm a {this.props.name}! and i am {this.props.age}
				</p>
				<p>{this.props.children}</p>
				<input
					ref={this.inputElement}
					type="text"
					onChange={this.props && this.props.onChangeText}
					value={this.props.name}
				/>
				<button onClick={this.props && this.props.onClickDelete}>Delete</button>
			</React.Fragment>
		);
	}
}

export default withClass(Person, 'person');
