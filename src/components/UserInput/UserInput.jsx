import React from 'react';

const UserInput = props => {
	return (
		<input
			type="text"
			onChange={props.onInputChange}
			value={props.defaultValue}
		/>
	);
};

export default UserInput;
