import React, { Component } from 'react';

import UserInput from './UserInput/UserInput';
import TextValidation from './TextValidation/TextValidation';
import CharComponent from './CharComponent/CharComponent';

class App3 extends Component {
	state = {
		inputText: ''
	};

	handleChangeInput = event => {
		this.setState({
			inputText: event.target.value
		});
	};

	handleRemovechar = index => e => {
		const arrInputText = this.state.inputText.split('');

		arrInputText.splice(index, 1);

		this.setState({
			inputText: arrInputText.join('')
		});
	};

	renderChar = () => {
		const arrInputText = this.state.inputText.split('');

		return arrInputText.map((letter, index) => (
			<CharComponent
				key={index}
				letter={letter}
				onClickChar={this.handleRemovechar(index)}
			/>
		));
	};

	render() {
		return (
			<div style={{ textAlign: 'center' }}>
				<h1>Assignment</h1>
				<UserInput
					onInputChange={this.handleChangeInput}
					value={this.state.inputText}
				/>
				<TextValidation inputText={this.state.inputText} />
				{this.renderChar()}
			</div>
		);
	}
}

export default App3;
