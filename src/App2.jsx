import React, { useState } from 'react';

import UserInput from './UserInput/UserInput';
import TextValidation from './TextValidation/TextValidation';
import CharComponent from './CharComponent/CharComponent';

const App2 = props => {
	const [inputTextState, setInputText] = useState({
		inputText: ''
	});

	const handleChangeInput = event => {
		setInputText({
			inputText: event.target.value
		});
	};

	const handleRemovechar = index => e => {
		const arrInputText = inputTextState.inputText.split('');

		arrInputText.splice(index, 1);

		setInputText({
			inputText: arrInputText.join('')
		});
	};

	const renderChar = () => {
		const arrInputText = inputTextState.inputText.split('');

		return arrInputText.map((letter, index) => (
			<CharComponent
				key={index}
				letter={letter}
				onClickChar={handleRemovechar(index)}
			/>
		));
	};

	console.log(inputTextState.inputText);

	return (
		<div style={{ textAlign: 'center' }}>
			<h1>Assignment</h1>
			<UserInput
				onInputChange={handleChangeInput}
				defaultValue={inputTextState.inputText}
			/>
			<TextValidation inputText={inputTextState.inputText} />
			{renderChar()}
		</div>
	);
};

export default App2;
