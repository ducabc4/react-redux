import React, { Component } from 'react';

import Persons from '../components/Persons/Persons';
import ErrorBoundary from '../components/ErrorBoundary/ErrorBoundary';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import AuthContext from '../context/auth-context';

import './App.css';

class App extends Component {
	constructor() {
		super();

		this.state = {
			persons: [
				{ id: 1, name: 'Thanh', age: 23 },
				{ id: 2, name: 'Nam', age: 23 },
				{ id: 3, name: 'Loan', age: 24 }
			],
			showPerson: false,
			isAuth: false
		};
	}

	handleToggleShowPerson = () => {
		this.setState((prevState, props) => {
			return { showPerson: !prevState.showPerson };
		});
	};

	handleDeletePerson = id => e => {
		const personIndex = this.state.persons.findIndex(p => p.id === id);

		const persons = [...this.state.persons];
		persons.splice(personIndex, 1);

		this.setState({
			persons
		});
	};

	handleChangeName = id => e => {
		const persons = [...this.state.persons];
		const personIndex = persons.findIndex(p => p.id === id);
		persons[personIndex].name = e.target.value;

		this.setState({
			persons
		});
	};

	handleClickLogin = () => {
		this.setState(prevState => {
			return {
				isAuth: !prevState.isAuth
			};
		});
	};

	render() {
		return (
			<React.Fragment>
				<button onClick={this.handleHideCockpit}>hide cokpit</button>
				<AuthContext.Provider
					value={{
						isAuth: this.state.isAuth,
						onClickLogin: this.handleClickLogin
					}}
				>
					<Cockpit
						showPerson={this.state.showPerson}
						onToggleShowPerson={this.handleToggleShowPerson}
					/>
					{this.state.showPerson && (
						<ErrorBoundary>
							<Persons
								handleDeletePerson={this.handleDeletePerson}
								handleChangeName={this.handleChangeName}
								persons={this.state.persons}
							/>
						</ErrorBoundary>
					)}
				</AuthContext.Provider>
			</React.Fragment>
		);
	}
}

export default withClass(App, 'App');
