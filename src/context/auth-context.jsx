import React from 'react';

const authContext = React.createContext({
	isAuth: false,
	onClickLogin: () => {}
});

export default authContext;
